from PIL import Image
import json
import random
import os
from pinatapy import PinataPy

IMAGES_PATH = os.environ.get('IMAGES_PATH', './input/') 
RESULT_PATH = os.environ.get('RESULT_PATH', './static/') 
NUMBER_OF_ITEMS = os.environ.get('NUMBER_OF_ITEMS', 100)
BASEURL = os.environ.get('BASEURL', 'http://54.219.243.40')
IMG_WIDTH_HEIGHT = 640

def get_missing_images(attributes):
    missing_files = {}
    counter = 0
    for attribute,v in attributes.items():
        for i in list(v.keys()):
            if(not os.path.isfile(IMAGES_PATH + attribute + '/' + i + '.png')):
                counter = counter + 1
                json = {str(counter): IMAGES_PATH + attribute + '/' + i + '.png'}
                missing_files.update(json)
    return missing_files

def generate_image(attributes,id):
    pinata = PinataPy('c4d82043bf926cdf582c', 'a133b88443a836a115a546bb1e802385ee46e3f402831a5b02a38fec562ecdc0')
    print('Generating img #' + str(id))
    
    image_data = {}
    image_data['name'] = "xiod #" + str(id)
    attributes_values = []

    base = Image.new('RGBA',(IMG_WIDTH_HEIGHT,IMG_WIDTH_HEIGHT))
    for attribute,v in attributes.items():
        # Get the winning attribute
        attr = random.choices(list(v.keys()), weights=list(v.values()))[0]

        image_path = IMAGES_PATH + attribute + '/' + attr + '.png'
        
        if(not os.path.isfile(image_path)):
            json_obj = {"trait_type": attribute,"value":  "Missing " + image_path}
            attributes_values.append(json_obj)
            continue

        # Open the image of the winning attr
        image = Image.open(image_path)

        # paste the image on the base image (transparent paster)
        base.alpha_composite(image)

        # Add winning attribute to the image data
        json_obj = {"trait_type": attribute,"value": attr}
        attributes_values.append(json_obj)

    image_name = str(id) + ".png"
    json_name =  str(id) + ".json"

    base.save(RESULT_PATH + image_name)
    image_data['image'] = "https://gateway.pinata.cloud/ipfs/" + pinata.pin_file_to_ipfs(RESULT_PATH + image_name)['IpfsHash']
    image_data['attributes'] = attributes_values

    with open(RESULT_PATH + '/json/' + json_name, 'w') as file:
        json.dump(image_data, file,indent=4)

    return(image_data)

def generate(attributes):   
    all_data = {}

    for i in range(0,NUMBER_OF_ITEMS):
        img_data = generate_image(attributes,i)
        json = {str(i): img_data}
        all_data.update(json)

    return all_data,200