import sqlite3

con = sqlite3.connect('example1.db')
c = con.cursor()


def init():
    c.execute(''' SELECT count(name) FROM sqlite_master WHERE type='table' AND name='metadata' ''')

    #if the count is 1, then table exists
    if c.fetchone()[0]==1 : 
        print('Table exists.')
    else :
        c.execute('''CREATE TABLE metadata
           (id INTEGER PRIMARY KEY,
            image text,
            name text,
            Environment text,
            Suit text,
            Genetix text,
            Helmet text,
            Badge text,
            Visor text,
            Wings text,
            Gender text,
            Artifact text,
            Mutation text)
            ''')

def insert():
    # rows = [('2006-03-28', 'BUY', 'IBM', 1000, 45.00),
    #     ('2006-04-05', 'BUY', 'MSOFT', 1000, 72.00),
    #     ('2006-04-06', 'SELL', 'IBM', 500, 53.00)]
    row = ("xiod #1", "https://gateway.pinata.cloud/ipfs/Qme4oDcpyvaejpjxuC6VcgWUMPa9pf6FVc3V8gmftkxyag","Azure","Poseidon","Xara_Tek", "Super_Basic_Helm","Ruby_001","Missing ./input/Visor/NO_VISOR.png","Sol","XX","WAGMI","Missing ./input/Mutation/NONE.png")
    c.execute('insert into metadata values (null,?,?,?,?,?,?,?,?,?,?,?,?)', row)
    con.commit()

def read():
    for row in c.execute('SELECT * FROM metadata'):
        print(row)

init()
insert()
read()

