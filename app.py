from flask import Flask,render_template
from input import attributes
from nft_gen import *

app = Flask(__name__)

@app.route('/')
def hello_world():
    return get_missing_images(attributes)

@app.route("/generate")
def gen():
    return generate(attributes)

@app.route("/generate/<nft_id>")
def gen1(nft_id):
    return generate_image(attributes,nft_id)

@app.route('/metadata/<nft_id>')
def page(nft_id):
    try:
        attributes = open(RESULT_PATH + 'json/' + str(nft_id) + '.json', 'r').read()
        return json.loads(attributes)
    except FileNotFoundError:
        return 'File not found',404    

@app.route("/stat/<id>")
def show(id):
    files = [ f for f in os.listdir(RESULT_PATH) if os.path.isfile(os.path.join(RESULT_PATH,f)) ]
    image = random.choice(files)
    try:
        attributes = open(RESULT_PATH + 'json/' + os.path.splitext(id)[0]+ '.json', 'r').read()
        return render_template('random.html',data=image,nextid = "./" +str(int(id) + 1), jsonfile=json.dumps(attributes))
    except FileNotFoundError:
        return 'File not found',404  

@app.route("/random")
def get():
    files = [ f for f in os.listdir(RESULT_PATH) if os.path.isfile(os.path.join(RESULT_PATH,f)) ]
    image = random.choice(files)
    try:
        attributes = open(RESULT_PATH + 'json/' + os.path.splitext(image)[0]+ '.json', 'r').read()
        return render_template('random.html',data=image, jsonfile=json.dumps(attributes))
    except FileNotFoundError:
        return 'File not found',404  
    

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080, debug=True)